# token_example

Testing [AIP #01](https://github.com/aionnetwork/AIP/issues/3) to implement an ERC20-compatible token on the AION blockchain

## Requirements

- [Node](https://nodejs.org)
- Titan CLI: `npm i -g @titan-suite/cli`
- (optional) yarn: `npm i -g yarn`

## Setup

- `yarn` or `npm i` to install project dependencies
- open the `titanrc.js` config file
    ```
        module.exports = {
            "host": "http://127.0.0.1",
            "port": 8545,
            "defaultAccount": "",
            "password": ""
        }
    ```
    - breakdown:
        - `host` is the IP address of your AION node
        - `port` is where the JSON-RPC is exposed on your node. *(Combined with `host`, this is used as your Web3 HTTP Provider).*
        - `defaultAccount` is a wallet address of your choice you control on the node
        - `password` will be used to unlock the account.
- update the values as required

## Run

- Deploy contracts:
    - `titan deploy contracts/TestToken.sol` and select *TestToken* in the drop down
    - or `titan deploy contracts/TestToken.sol -n TestToken`
- `yarn test`
