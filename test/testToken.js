import { expect } from "chai"
import {
  unlock,
  mainAccount,
  mainAccountPass,
  web3
} from '../common.js'
import { abi, deployed_address } from '../build/bolts/TestToken.json'


let acc1 = mainAccount
let acc2 = web3.personal.listAccounts[1]
let pwd = mainAccountPass

let contractInstance

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

describe('a] Setup', () => {
  it('should unlock the accounts', async () => {
    const unlocked = await unlock(web3, acc1, pwd)
    expect(unlocked).to.equal(mainAccount)
  })

  it('should use the deployed contract', async () => {
    contractInstance = web3.eth.contract(abi).at(deployed_address)
    const res = await contractInstance.totalSupply.call({ from: acc1, gas: 1500000 })
    const supply = Number(res)

    expect(supply).to.equal(10000000)
  }).timeout(0)

  it('should get the right balance of acc1', async () => {
    const res = await contractInstance.balanceOf.call(acc1, { from: acc1, gas: 1500000 })
    const balance = Number(web3.fromWei(res, 'ether'))

    expect(balance).to.equal(10000000)
  }).timeout(0)

  it('should transfer to acc2 successfully', async () => {
    const txHash = await contractInstance.transfer(
      acc2,
      web3.toWei(200, 'ether'),
      { from: acc1, gas: 1500000 }
    )
    await sleep(30e3)
    const txReceipt = web3.eth.getTransactionReceipt(txHash)

    expect(txReceipt).to.not.be.null
  }).timeout(0)

  it('should get the right balance of acc2', async () => {
    const res = await contractInstance.balanceOf.call(acc2, { from: acc2, gas: 1500000 })
    const balance = Number(web3.fromWei(res, 'ether'))

    expect(balance).to.equal(200)
  }).timeout(0)

})
